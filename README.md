# youtube-to-podcast
Have you ever wanted to use your podcast player with youtube videos? No?
Well I did, so here we are.

`youtube-to-podcast` is a small service that will download videos from youtube
and stream them as audio files. Assuming you are hosting it at
`youtube-to-podcast.example.com`, two endpoints will be available:

- `http://youtube-to-podcast.example.com/audio/<video-id>.m4a`
- `http://youtube-to-podcast.example.com/feed/<channel-id>`

(yes, HTTP. Use a reverse proxy. `youtube-to-podcast` only accepts connection
from `localhost`.)

When you request `/audio/<video-id>.m4a`, `youtube-to-podcast` downloads the
`<video-id>` with `yt-dlp` as an audio file, and returns it as a response.
You must be
[authenticated](https://en.wikipedia.org/wiki/Basic_access_authentication) to
access this.

Requesting `/feed/<channel-id>` returns a slightly modified version of
`https://www.youtube.com/feeds/videos.xml?channel_id=<channel-id>` that makes
it usable by podcast clients by directing them to `/audio/<video-id>.m4a`.

## Caveats
- This is an **hack**. Don't expect it to work reliably. Or at all.
- Every operation is synchronous, *including downloads from youtube*. I told
  you it was an hack.
- As a result, `youtube-to-podcast` is suitable for use by ≤1 users. Only the
  first line of the credentials file is read, to help you resist the temptation
  of allowing other users. Thank me later.
- Additionaly, depending on how fast you can download videos, your podcast
  player may get tired of waiting and close the connection.
  `youtube-to-podcast` will nevertheless finish the download before doing
  anything else, so just be patient and try again later.

## Installation
### Requirements
- yt-dlp;
- FFmpeg;
- Guile;
- A reverse proxy to handle TLS.

### Setup
This is how I set everything up in Alpine, it should be easy to adjust for
other distros:

```sh
# use your package manager
apk add yt-dlp ffmpeg guile

# you may have useradd/groupadd instead of adduser/addgroup
addgroup ytpod
adduser -h /var/lib/ytpod -s /usr/bin/nologin -D -S -G ytpod ytpod

cd /var/lib/ytpod
git clone https://codeberg.org/dalz/youtube-to-podcast

cp youtube-to-podcast/youtube-to-podcast /usr/local/bin/

# if you use OpenRC:
cp openrc/init.d/youtube-to-podcast /etc/init.d
cp openrc/conf.d/youtube-to-podcast /etc/conf.d

# replace USERNAME and PASSWORD with appropriate values
printf "USERNAME:PASSWORD" | base64 > /var/lib/ytpod/auth

mkdir /var/cache/ytpod
chown -R ytpod:ytpod /var/lib/ytpod /var/cache/ytpod
chmod 750 /var/lib/ytpod
```

Here I created a new user for the service. This is optional but highly
recommended.

You also need to configure your reverse proxy before running
`youtube-to-podcast`.

### Reverse proxy configuration
With Caddy:

```
# /etc/caddy/Caddyfile
youtube-to-podcast.example.com {
	reverse_proxy http://localhost:5401
}
```

### Running the service
Take a look at the configuration options:

```
$ youtube-to-podcast --help
youtube-to-podcast [options]

    -h, --host <HOST>
            Required. Used to build urls for audio files, as in:

                https://<HOST>/audio/<video_id>.m4a

    -p, --port <PORT>
            Defaults to 5401.

    --cache-dir <PATH>
            Where to store downloaded audio files.
            Defaults to /var/cache/ytpod, which should be created before
            executing the program.

    --cache-ttl <DAYS>
            When the /feed endpoint is accessed, any dowloaded file older than
            <DAYS> (default 3) is deleted.

    --credentials-file <PATH>
            Path to a file (default /var/lib/ytpod/auth) to be used to
            authenticate the user. Create it as follows:

                printf username:password | base64 > /var/lib/ytpod/auth

    --yt-feed-url <URL>
            Defaults to

                https://www.youtube.com/feeds/videos.xml?channel_id=

            This option is useful if you want to use invidio.us or disable HTTPS.

    --help
          Print this message.
```

If you created the `ytpod` user, you could run `youtube-to-podcast` as follows:

```
# su ytpod -s /bin/sh -c 'youtube-to-podcast -h youtube-to-podcast.example.com'
```

On Alpine, Guile can't HTTPS, so as a workaround I'm doing this:

```
# su ytpod -s /bin/sh -c 'youtube-to-podcast.scm -h youtube-to-podcast.example.com --yt-feed-url http://www.youtube.com/feeds/videos.xml?channel_id='
```

(notice the `http` instead of `https`)

#### OpenRC
If you ran:

```
# cp openrc/init.d/youtube-to-podcast /etc/init.d
# cp openrc/conf.d/youtube-to-podcast /etc/conf.d
```

you can just edit `/etc/conf.d` and then start `youtube-to-podcast` with:

```
# rc-service youtube-to-podcast start
# rc-update add youtube-to-podcast # run at boot
```
